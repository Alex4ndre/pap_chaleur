#pragma once
class materiaux
{
	double lambda_;
	double ro_;
	double c_;

public:
	/**
	 * \brief materiaux
	 * Contructeur de la classe materiaux
	 * \param	double lambda
				double ro
				double c
	*/
	materiaux(double,double,double);

	/**
	 * \brief get_lambda
	 * getter lambda	 
	*/
	int get_lambda();
	/**
	 * \brief get_ro
	 * getter ro
	*/
	int get_ro();
	/**
	 * \brief get_c
	 * getter c
	*/
	int get_c();
};

