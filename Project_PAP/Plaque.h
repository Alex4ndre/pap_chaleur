#pragma once




class Plaque
{
	int  points_;
	double  tmax_;
	double  L_;
	double lambda_;
	double ro_;
	double c_;

	double* temps_;
	double* x_;
	double* y_;


public:
	/**
	 * \brief Plaque
	 * Contructeur de la classe Plaque
	 * \param	int const : le nombre de points (discretisation)
				double const : temps d'observation
				double const : longueur des côtés de la plaque
				double const : valeur de f (source de chaleur)
				double const : conductivité thermique du matériaux
				double const : masse volumique
				double const : chaleur massique
	*/
	Plaque(int const, double const, double const, double const, double const, double const, double const);
	
	/**
	 * \brief get_points
	 * Getter pour obtenir le nombre de points de discrétisation
	 * \param	
	 */
	
	int get_points() const;
	double* F_;
};


