#include <iostream>
#include "Barre.h"
#include "thomas_barre.h"
#include "materiaux.h"
#include <string>
#include <fstream>
int main() {
	
	// Discretisation de l'intervalle de temps tmax=16s et espace L=1m en 1001 points
	int const points = 1001;
	double const tmax = 16;
	double const L = 1;
	// Cr�ation de la fonction f
	double const f = 80 + 273.15; // en Kelvin
	// Donn�es des mat�riaux
	

	materiaux cuivre = materiaux(389, 8940, 380);
	materiaux fer = materiaux(80.2, 7874, 440);
	materiaux verre=materiaux (1.2, 2530, 840);
	materiaux polystyrene = materiaux(0.1, 1040, 1200);

	//Creation d'une barre
	Barre b_cuivre(points, tmax, L, f, cuivre.get_lambda(), cuivre.get_ro(), cuivre.get_c());
	Barre b_fer(points, tmax, L, f, fer.get_lambda(), fer.get_ro(), fer.get_c());
	Barre b_verre(points, tmax, L, f, verre.get_lambda(), verre.get_ro(), verre.get_c());
	Barre b_polystyrene(points, tmax, L, f, polystyrene.get_lambda(), polystyrene.get_ro(), polystyrene.get_c());
	// Cr�ation matrice de Thomas - Cuivre
	double dx = L / 1000;
	double dt = tmax / 1000;
	double A_cuivre = (dt * cuivre.get_lambda()) / (pow(dx, 2) * cuivre.get_ro() * cuivre.get_c());
	double** M_cuivre = matrice_thomas(points - 2, 1 + 2 * A_cuivre, -A_cuivre, -A_cuivre);

	// Cr�ation matrice de Thomas - Fer
	double A_fer= (dt * fer.get_lambda()) / (pow(dx, 2) * fer.get_ro() * fer.get_c());
	double** M_fer = matrice_thomas(points - 2, 1 + 2 * A_fer, -A_fer, -A_fer);

	// Cr�ation matrice de Thomas - Verre
	double A_verre = (dt * verre.get_lambda()) / (pow(dx, 2) * verre.get_ro() * verre.get_c());
	double** M_verre = matrice_thomas(points - 2, 1 + 2 * A_verre, -A_verre, -A_verre);
	// Cr�ation matrice de Thomas - Polystyrene
	double A_polystyrene = (dt * polystyrene.get_lambda()) / (pow(dx, 2) * polystyrene.get_ro() * polystyrene.get_c());
	double** M_polystyrene = matrice_thomas(points - 2, 1 + 2 * A_polystyrene, -A_polystyrene, -A_polystyrene);

	// Initialisation du vecteur initial
	int u0 = 13 + 273.15;//en kelvin
	double* u;
	u = new double[points];
	for (int i = 0;i < points;i++) {
		u[i] = u0;
	}
	// Matrice contenant les resultats
	double** Res_barre;
	Res_barre = new double* [points];
	for (int i = 0; i < points; i++) {
		Res_barre[i] = new double[points];
	}
	// Algrotithme de r�solution exemple avec polystyrene
	
	for (int i = 0;i < points;i++) {
		for (int j = 0;j < points;j++) {
			Res_barre[j][i] = u[j];
		}
		u=resolution(M_polystyrene, u, points, 1 + 2 * A_polystyrene, -A_polystyrene, b_polystyrene.F_);
	}
	std::string const nomFichier("C:/Users/Alexandre/Desktop/PAP_polystyrene.txt");//mon chemin 
	std::ofstream monFlux(nomFichier.c_str());
	for (int i = 0;i < points;i++) {
		for (int j = 0;j < points; j++) {
			monFlux << Res_barre[i][j] <<";";
		}
		monFlux << std::endl;
		
	}
	

}