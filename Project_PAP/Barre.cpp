#include "Barre.h"

#include "math.h"
#include <iostream>

Barre::Barre(int const points,double const tmax,double const L,double const f, double const lambda, double const ro, double const c) // constructeur defaut
{
	points_ = points;
	tmax_ = tmax;
	L_ = L;
	lambda_ = lambda;
	ro_ = ro;
	c_ = c;
	x_ = new double[points];
	temps_ = new double[points];
	F_ = new double[points];
	for (int i = 0;i < points;i++) { // Discretisation x et t
		temps_[i] = tmax * i / 1000;
		x_[i] = 1.0 * i / 1000;
	};
	for (int i = 0;i < points;i++) {
		if (x_[i] >= L / 10 && x_[i] <= 2 * L / 10) { 
			F_[i] = (tmax * f * f)*(tmax/(points-1.0))/(ro_*c_);
			
		}
		if (x_[i] >= L * 5 / 10 && x_[i] <= L * 6 / 10) {
			F_[i] = ((3.0 / 4.0) * tmax * f * f)*(tmax / (points - 1.0)) / (ro_ * c_);
		}
		if ((x_[i] < L / 10)||(x_[i]<5*L/10 && x_[i]>2*L/10) || (x_[i]>6*L/10)) {
			F_[i] = 0;
		}
	}
	

}


int Barre::get_points() const{
	return points_;
}




