#include "thomas_barre.h"
#include <iostream>

double** matrice_thomas(int const size, double diag, double diag_sup, double diag_inf) {
	double** M;
	/* Allocation*/
	M = new double* [size];
	for (int i = 0; i < size; i++) {
		M[i] = new double[size];
	}
	/*Initialisation*/
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (i == j) {
				M[i][j] = 1;
			}
			if (i+1 == j) {
				if (i == 0) { // premiere ligne
					M[i][j] = diag_sup / diag;
				}
				else {
					M[i][j] = (diag_sup) / (diag - M[i - 1][j - 1] * diag_inf);
				}
			}
			if (i != j && (i+1) != j) {
				M[i][j] = 0;
			}
		}
	}
	return(M);
}

double* resolution(double** M, double* u_ante, int const points, double diagM_init, double diagsupM_init, double* F) {
	// Ajout de la source de chaleur
	for (int k = 0;k < points;k++) {
		u_ante[k] = u_ante[k] + F[k];
	}
	
	// on rearrange u_ante
	for (int k = 1;k < points - 2;k++) { // sans le 0 et le dernier
		if (k == 1) {
			u_ante[k] = u_ante[k] / diagM_init;
		}
		else {
			u_ante[k] = (u_ante[k] - u_ante[k - 1] * diagsupM_init) / (diagM_init - M[k - 2][k - 1]*diagsupM_init);
		}
	}
	//std::cout << "apres modif "<<u_ante[points - 4] << std::endl;
	double* u_new;
	/* Allocation*/
	u_new = new double[points];
	for (int k = points - 2;k > 0;k--) {
		if (k == (points - 2)) { // si derniere composante du vecteur de temperature (sans comprendre celle qui touche dehors)
			u_new[k] = u_ante[k];
		}
		else {
			u_new[k] = u_ante[k] - M[k][k + 1] * u_new[k + 1];
		}
	}
	for (int i = 0;i < points;i++) {
		if (u_new[i] < 286) {
			u_new[i] = 286;
		}
	}
	u_new[points - 1] = u_ante[points - 1]; //u0
	u_new[0] = u_ante[0]; //u0 calcul avec l'ennonce puis derivation
	
	return(u_new);
}

