#include "materiaux.h"

materiaux::materiaux(double lambda, double ro, double c) {
	lambda_ = lambda;
	ro_ = ro;
	c_ = c;
}

int materiaux::get_lambda() {
	return(lambda_);
}

int materiaux::get_ro() {
	return(ro_);
}

int materiaux::get_c() {
	return(c_);
}