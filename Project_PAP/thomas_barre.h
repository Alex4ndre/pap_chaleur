#pragma once
/**
	 * \brief matrice_thomas
	 * Cr�er une matrice de diagonale 1, et de coefficient superieur les lambda_i du rapport, � partir de la matrice tri-diagonale
	 * \param un entier pour la taille, les coeffs de la matrice tri-diagonale initiale
	*/
double** matrice_thomas(int const, double, double, double);

/**
	 * \brief resolution
	 * Applique l'algorithme de Thomas pour obtenir les temp�rature de la barre pour tout x � l'instant suivant
	 * \param double** : une matrice (matrice_thomas), double* temperature � l'�tape pr�c�dente, int const : la taille du vecteur de temp�rature, double et double : les coeffs de la matrice tri-diag initiale, double* : la source de chaleur F*(dt/ro*c)
	*/
double* resolution(double**, double*, int const, double , double , double* );